# README #

Guide pour déployer et tester l'application 

## Déploiement ##
### mongodb ###
L'application a une dépendence sur une base de données mongodb. Une instnace de la base de données doit
donc rouler afin que l'application fonctionne.

Par défaut, l'application tentera de se connecter à une instance non sécurisée sur le port 27017 du localhost.
La bd utilisée sera Notarius-java-challenge.

Il est possible de spécifier des identifiants pour une base de données qui serait protégée par mot de passe,
ainsi que d'autre valeurs pour le nom de la base de données, l'hôte et le port sous `src/main/resources/application.properties`

### maven ###

Copier le code source en local: `git clone https://sicamath@bitbucket.org/sicamath/java-challenge.git`

Lancer l'application: `./java-challenge/mvnw spring-boot:run`

C'est tout! L'application roule maintenant sur le port `8080`

### À partir du jar fourni ###

Un jar est également fournit à la racine du code source. Il suffit alors de l'exécuté après avoir cloné le repository.

`git clone https://sicamath@bitbucket.org/sicamath/java-challenge.git`


`java-challenge/notarius-challenge-0.0.1-SNAPSHOT.jar`

## API ##

Voici en bref une définition de l'API

### Créer un url raccourci ###
Pour créer un url raccourci, utiliser l'enpoint `/v1/Url`

Le corps de requête est un objet json contenant l'url à raccourcir. L'url doit être valide.
`
{
    "url": "https://www.notarius.com"
}
`

L'entête doit contenir la paire suivante: `Content-Type:application/json`

Le corps de réponse sera un objet json contenant un LinkId. C'est ce lien qui pourra être utilisé par la suite pour obtenir l'url complète (voir section suivante). Le lien fourni dans la réponse peut également être utilisé tel quel.
`
{
    "linkId": "2662520210",
    "_links": {
        "self": {
            "href": "http://localhost:8080/2662520210"
        }
    }
}
` 

### Obtenir un url raccourci ###
Pour obtenir un url précédement raccourci, utiliser l'enpoint `/{LinkId}` où LinkId est la valeur obtenue précédement.

L'entête doit contenir la paire suivante: `Content-Type:application/json`

Le corps de réponse contiendra un objet json avec l'url complète précédement fournie.
`
{
    "url": "https://www.notarius.com/",
    "_links": {
        "self": {
            "href": "http://localhost:8080/2116542163"
        }
    }
}
`

Un LinkId qui ne correspond à aucune url précédement raccourcie retournera un code de statut http 404.

### Obtenir tous les url raccourcis ###
Pour obtenir tous les url précédement raccourcis, utiliser l'enpoint `/v1/Links`

L'entête doit contenir la paire suivante: `Content-Type:application/json`

Le corps de réponse contiendra un tableau contenant tous les liens créés.


## Tester ##

Pour tester, deux méthodes sont présentées ici, mais n'importe quel client http est un candidat acceptable.

### Postman ###
Une collection postman est fournie à la racine du projet. Elle peut être importée tel quelle et utilisée pour tester l'API.

### curl ###
Voici quelques exemples d'appels curls pour tester chacun des enpoints mentionnées plus haut.

Créer un url raccourci :
`
curl -X POST \
  http://localhost:8080/v1/Url \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -d '{
    "url": "https://www.notarius.com"
}'
`

Obtenir un url raccourci :
`
curl -X GET \
  http://localhost:8080/4224695516 \
  -H 'cache-control: no-cache'
`

Obtenir tous les url raccourcis:
`
curl -X GET \
  http://localhost:8080/v1/Links \
  -H 'cache-control: no-cache' \
  -H 'postman-token: 77915c27-fa4b-f188-f803-c12aeea97b17'
`