package com.test.notariuschallenge;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.notariuschallenge.api.v1.model.LongUrl;


@SpringBootTest
@AutoConfigureMockMvc
class NotariusChallengeApplicationTests {

	@Autowired
	private MockMvc mvc;
	
	@Test
	void minifyValidUrl() throws Exception {
		LongUrl url = new LongUrl();
		url.setLongUrl("https://www.notarius.com/");
		mvc.perform(MockMvcRequestBuilders.post("/v1/Url")
				.content(asJsonString(url))
				.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.linkId").value("2116542163"));
	}
	
	@Test
	void minifyInvalidUrl() throws Exception {
		LongUrl url = new LongUrl();
		url.setLongUrl("RandomString");
		mvc.perform(MockMvcRequestBuilders.post("/v1/Url")
				.content(asJsonString(url))
				.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest());
	}	
	
	@Test
	void minifyLongUrl() throws Exception {
		LongUrl url = new LongUrl();
		url.setLongUrl("https://www.google.com/maps/place/Notarius/@45.5009138,-73.5608177,17z/data=!3m1!4b1!4m5!3m4!1s0x4cc91a5b2d0a6c2b:0x3e7ea87083f6a0aa!8m2!3d45.5009101!4d-73.558629");
		mvc.perform(MockMvcRequestBuilders.post("/v1/Url")
				.content(asJsonString(url))
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.linkId").value("1900511118"));
	}
	
	@Test
	void retrieveValidUrl() throws Exception {
		minifyValidUrl();
		mvc.perform(MockMvcRequestBuilders.get("/v1/Links/2116542163"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.url").value("https://www.notarius.com/"));;
	}
	
	@Test
	void retrieveUrlTooLong() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/v1/Links/012345678912346"))
			.andExpect(status().isBadRequest());;
	}
	
	@Test
	void retrieveUrlAlphaNum() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/v1/Links/012abc"))
			.andExpect(status().isBadRequest());;
	}
	
	@Test
	void retrieveUnkownUrl() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/v1/Links/123"))
			.andExpect(status().isNotFound());;
	}
	
	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}  

}
