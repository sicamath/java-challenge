package com.test.notariuschallenge;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.notariuschallenge.data.Link;
import com.test.notariuschallenge.data.LinkRepository;
import com.test.notariuschallenge.exception.EntityNotFoundException;

/**
 * 
 * @author Mathieu
 * 
 * This service creates and fetches minified urls.
 * 
 * A minified url (link), is generated from a valid url and consists of a hash of maximum 10 chars long of
 * the input. An url will always produce the same link.
 *
 */
@Service
public class UrlShortenerService {  
	
	@Autowired
	private LinkRepository repository;
	
	private Logger logger = LoggerFactory.getLogger(UrlShortenerService.class);
	
	/**
	 * Creates a minified version of the URL
	 * 
	 * @param IncomingUrl The url that was requested to be minified
	 * @return The LinkId representing the url
	 * @throws MalformedURLException - if the given url is invalid
	 */
	public int minifyUrl(String IncomingUrl) throws MalformedURLException {
		logger.info("Minifying URL " + IncomingUrl);
		
		// Vérifier si l'URL en input est valide. Ne pas attraper l'exception; le controlleur pourra retourner
		// l'information à l'utilisateur
        URL TestUrl =  new URL(IncomingUrl);
        
        // Créer un hash pour l'URL. 
        // En cas de collision, répéter la procédure sur la dernière
        // valeur du hash.
        String HashString = IncomingUrl;
        int LinkId;
        do {
            LinkId = HashString.hashCode();
            logger.debug("HashString " + HashString + " generated LinkId " + LinkId);
            HashString = Integer.valueOf(LinkId).toString();
        } while (!createLinkForUrl(LinkId, TestUrl));
                   
       return LinkId;
        
    }
    
	/**
	 * 
	 * Get the LongUrl object matching the LinkId. Throws EntityNotFoundException when the linkId 
	 * does not match an entry in the database.
	 * 
	 * @param linkId the link id identifying the long url
	 * @return the LongUrl object.
	 * @throws EntityNotFoundException 
	 */
    public Link getLink(int linkId) throws EntityNotFoundException {
    	logger.info("Retrieving link " + linkId);
    	// Obtenir le lien existant, si dans la BD
		Link tryLink = repository.findByLinkId(linkId);
		if (tryLink != null) {
	    	logger.debug("Found " + tryLink);
			return tryLink;
		}

    	logger.debug("No URL matching " + linkId + " was found");
    	throw new EntityNotFoundException("No URL matching " + linkId + " was found");
    }
    
	/**
	 * 
	 * Creates the short link in the database
	 * 
	 * @param linkId The short link to create
	 * @param incomingUrl The url for which the short link must be created
	 * @return true if the link was created or if an existing record for the same url was given. 
	 * 		   false otherwise
	 * @throws MalformedURLException 
	 */
    private boolean createLinkForUrl(int linkId, URL incomingUrl) throws MalformedURLException {
    	logger.info("Creating link " + linkId);
		// Obtenir le lien existant, si dans la BD
		Link tryLink = repository.findByLinkId(linkId);
		if (tryLink != null) {
			// le lien existe déjà, est-ce pour le même fichier?
			if (incomingUrl != null && incomingUrl.sameFile(new URL(tryLink.url))) {
				logger.debug("Found matching link" + tryLink);
				return true;
			} else {
				logger.debug("Found collision" + tryLink);
				return false;
			}
		} else {
			// créer un nouveau lien
			Link link = new Link(linkId, incomingUrl.toString());
			logger.info("Inserting link in database : " + linkId);
			repository.insert(link);
			return true;
		}
           
    }

    /**
     * 
     * Gets all the links in the database 
     * 
     * @return a list of all the links
     */
	public List<Link> getAllLinks() {
		return repository.findAll();
	}

}
