package com.test.notariuschallenge.exception;

/**
 * 
 * @author Mathieu
 * 
 * Class to be thrown when an entity was requested by the user, but is not found in the database.
 *
 */
public class EntityNotFoundException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String message) {
        super(message);
    }
}
