package com.test.notariuschallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotariusChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotariusChallengeApplication.class, args);
	}

}
