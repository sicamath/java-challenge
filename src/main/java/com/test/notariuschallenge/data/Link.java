package com.test.notariuschallenge.data;

import org.springframework.data.annotation.Id;

/**
 * 
 * @author Mathieu
 * 
 * Persistedobjest in the database. Holds the minified version of an url.
 *
 */
public class Link {

	@Id
    private String id;
	
    public int linkId;
    public String url;
    
    public Link(int linkId, String url){
        this.linkId = linkId;
        this.url = url;
    }

}
