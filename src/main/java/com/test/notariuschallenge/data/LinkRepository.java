package com.test.notariuschallenge.data;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * 
 * @author Mathieu
 * 
 * Interface to generate a repository for Link objects. The implementation class will be auto-generated
 * by spring.
 *
 */
public interface LinkRepository extends MongoRepository<Link, String>{
	
	/**
	 * 
	 * Finds a link in the database.
	 * 
	 * @param LinkId The link id to fetch
	 * @return The link Object matching th LinkId, null of not in the database.
	 */
	public Link findByLinkId(int LinkId);

}
