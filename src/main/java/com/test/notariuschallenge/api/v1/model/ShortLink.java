package com.test.notariuschallenge.api.v1.model;

import org.springframework.hateoas.RepresentationModel;

/**
 * 
 * @author Mathieu
 * 
 *  * I/O class for the UrlShortenerClass. Represents an url in its minified format.
 *
 */
public class ShortLink extends RepresentationModel<LongUrl> {
	
    private int linkId;
    
    public ShortLink(int linkId){
        this.linkId = linkId;
    }
    
    @Override
    public String toString() {
        return Integer.toUnsignedString(this.linkId);
    }
    
    public String getLinkId() {
    	return this.toString();
    }
}
