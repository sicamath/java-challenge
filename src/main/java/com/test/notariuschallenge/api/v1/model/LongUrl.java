package com.test.notariuschallenge.api.v1.model;

import org.springframework.hateoas.RepresentationModel;

import com.test.notariuschallenge.data.Link;

/**
 * 
 * @author Mathieu
 * 
 * I/O class for the UrlShortenerClass. Represents an url in its original format.
 *
 */
public class LongUrl extends RepresentationModel<LongUrl> {
	
	private String url = "";
	
	public LongUrl(String url) {
		this.url = url;
	}
	
	public LongUrl() {
	}
	
	public static LongUrl toLongUrl(Link link) {
		return new LongUrl(link.url);
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setLongUrl(String url) {
		this.url = url;
	}

}
