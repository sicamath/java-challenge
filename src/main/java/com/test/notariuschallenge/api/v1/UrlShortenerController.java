package com.test.notariuschallenge.api.v1;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

import com.test.notariuschallenge.UrlShortenerService;
import com.test.notariuschallenge.api.v1.model.LongUrl;
import com.test.notariuschallenge.api.v1.model.ShortLink;
import com.test.notariuschallenge.data.Link;
import com.test.notariuschallenge.exception.EntityNotFoundException;

@RestController
public class UrlShortenerController {
	
    @Autowired
    private UrlShortenerService shortenerService;
	
	@PostMapping(value = "/v1/Url")
	@ResponseBody
	public ShortLink url(@RequestBody LongUrl longUrl) {
		try {
			ShortLink respBody = new ShortLink(shortenerService.minifyUrl(longUrl.getUrl()));
			respBody.add(linkTo(methodOn(UrlShortenerController.class).getShortLink(respBody.getLinkId())).withSelfRel());
			return respBody;
		} catch (MalformedURLException e) {
			throw new ResponseStatusException(
			          HttpStatus.BAD_REQUEST, "Not a valid url", e);
		}
	}
	
	@GetMapping(value = "/v1/Links/{linkId}")
	@ResponseBody
	public LongUrl getShortLinkV1(@PathVariable String linkId) {
		try {
			LongUrl respBody = LongUrl.toLongUrl(shortenerService.getLink((int) Integer.parseUnsignedInt(linkId)));
			respBody.add(linkTo(methodOn(UrlShortenerController.class).getShortLink(linkId)).withSelfRel());
			return respBody;
		} catch (NumberFormatException e) {
			throw new ResponseStatusException(
			          HttpStatus.BAD_REQUEST, "Not a number", e);
		} catch (EntityNotFoundException e) {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "The provided link does not exists", e);
		} catch (Exception e) {
			throw new ResponseStatusException(
			          HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong", e);
		}
	}
	
	@GetMapping(value = "/{linkId}")
	@ResponseBody
	public LongUrl getShortLink(@PathVariable String linkId) {
		return getShortLinkV1(linkId);
	}
	
	@GetMapping(value = "/v1/Links")
	@ResponseBody
	public List<LongUrl> getAllLinks() {

		List<Link> AllLinks = shortenerService.getAllLinks();
		List<LongUrl> AllUrls = new ArrayList<LongUrl>(AllLinks.size());
		for (Link link : AllLinks) {
			LongUrl url = LongUrl.toLongUrl(link);
			String LinkIdStr = Integer.toUnsignedString(link.linkId);
			url.add(linkTo(methodOn(UrlShortenerController.class).getShortLink(LinkIdStr)).withSelfRel());
			AllUrls.add(url);
		}
		
		return AllUrls;
	}

}
